const addProduto = (nome, fabricante, categoria, quantidade, valor, cont) => {
    
    let card_produto = document.createElement("div");

    card_produto.setAttribute("id", cont);
    card_produto.innerHTML = `
        <div class="lista-produtos">
            <h2 class="name">${nome}</h2>
            <h2>${fabricante}</h2>
            <h2>${categoria}</h2>
            <h2>${quantidade}</h2>
            <h2>${valor}</h2>

            <button id="delete" onclick="delete_card(${cont})">Deletar</button>
        </div>
    `;
    return card_produto;
}