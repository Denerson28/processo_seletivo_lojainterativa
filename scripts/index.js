const category = document.getElementById("category");
let categories = document.createElement("select");
categories.setAttribute("class", "register");
categories.setAttribute("id", "categories");
const addProduct = document.getElementById("addProduct");
const list_sequence = document.querySelector(".produtos");
const buttonDelete = document.getElementById("delete");
const search = document.getElementById("search");
let cont = 0;
const products_list = [];

function verify(opcao) {
    switch(opcao){
        case "1":
            category.innerHTML = "";
            categories.innerHTML = `
            <option value="1" selected="selected">categoria1 opcao1</option>
            <option value="2">categoria 2 opcao 1</option>
            <option value="3">categoria 3 opcao 1</option>`;
            category.append(categories);
            break;
        case "2":
                category.innerHTML = "";
                categories.innerHTML = `
                <option value="1" selected="selected">categoria1 opcao2</option>
                <option value="2">categoria 2 opcao 2</option>
                <option value="3">categoria 3 opcao 2</option>`;
                category.append(categories);
            break;
        case "3":
                category.innerHTML = "";
                categories.innerHTML = `
                <option value="1" selected="selected">categoria1 opcao3</option>
                <option value="2">categoria 2 opcao 3</option>
                <option value="3">categoria 3 opcao 3</option>`;
                category.append(categories);
            break;
        default:
            break;
    }
}

function fabricante_value() {
    let opcao = "1";
    let select = document.getElementById("select-fabricante");
    opcao = select.value;

    verify(opcao);
}

addProduct.addEventListener("click", (e) => {
    e.preventDefault();

    let name_product = document.getElementById("name").value;

    let fabricante_product = document.getElementById("select-fabricante");
    let fabricante_str = fabricante_product.options[fabricante_product.selectedIndex].text;

    let categoria_product = document.getElementById("categories");
    let categoria_str = categoria_product.options[categoria_product.selectedIndex].text;
    
    let quantidade = document.getElementById("quantidade").value;

    let valor = document.getElementById("valor").value;

    list_sequence.append(addProduto(name_product, fabricante_str, categoria_str, quantidade, valor, cont));

    product = {
        id: cont,
        name: name_product,
        fabricante: fabricante_str,
        categoria: categoria_str,
        quantidade: quantidade,
        valor: valor,
    }

    products_list.push(product);
    cont++;
})

function delete_card(cont) {
    let pos = 0;
    document.getElementById(cont).remove();

    products_list.forEach(element => {
        if(element.id == cont){
            products_list.splice(pos, 1);
        }
        pos++;
    });

    products_list.splice(pos, 1);
    console.log(products_list);
}

search.addEventListener("click", (e) => {
    e.preventDefault();
    list_sequence.innerHTML = "";
    let nameSearch = document.getElementById("buscar").value;
    products_list.forEach(element => {
        if(nameSearch === element.name){
            list_sequence.append(addProduto(element.name, element.fabricante, element.categoria, element.quantidade, element.valor, element.id));
        }
    });
})